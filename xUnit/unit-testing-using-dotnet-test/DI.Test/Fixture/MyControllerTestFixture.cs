﻿using DI.WebApi.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;

namespace DI.Test.Fixture
{
    public class MyControllerTestFixture : IDisposable
    {
        private readonly ServiceProvider _serviceProvider;

        public MyControllerTestFixture()
        {
            var services = new ServiceCollection();
            services.AddScoped<IService, MyService>(); // Register your services here
            _serviceProvider = services.BuildServiceProvider();
        }

        public MyController CreateController()
        {
            return ActivatorUtilities.CreateInstance<MyController>(_serviceProvider);
        }

        public void Dispose()
        {
            // Clean up any resources if needed
        }
    }
}
