﻿using Microsoft.AspNetCore.Mvc;
namespace DI.Test.Fixture
{
    public class MyControllerTests : IClassFixture<MyControllerTestFixture>
    {
        private readonly MyControllerTestFixture _fixture;

        public MyControllerTests(MyControllerTestFixture fixture)
        {
            _fixture = fixture;
        }

        [Fact]
        public async Task Get_Returns_Ok()
        {
            // Arrange
            var controller = _fixture.CreateController();

            // Act
            var result = await controller.Get();

            // Assert
            var okResult = Assert.IsType<OkObjectResult>(result);
            Assert.Equal("Test data", okResult.Value);
        }
    }
}
