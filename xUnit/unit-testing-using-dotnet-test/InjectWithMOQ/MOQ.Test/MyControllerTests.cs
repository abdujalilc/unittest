using Microsoft.AspNetCore.Mvc;
using Moq;
using MOQ.Controller.Controller;

namespace MOQ.Test
{
    public class MyControllerTests
    {
        [Fact]
        public async Task Get_Returns_Ok()
        {
            // Arrange
            var mockService = new Mock<IService>();
            mockService.Setup(service => service.GetDataAsync()).ReturnsAsync("Test data");

            var controller = new MyController(mockService.Object);

            // Act
            var result = await controller.Get();

            // Assert
            var okResult = Assert.IsType<OkObjectResult>(result);
            Assert.Equal("Test data", okResult.Value);
        }
    }
}