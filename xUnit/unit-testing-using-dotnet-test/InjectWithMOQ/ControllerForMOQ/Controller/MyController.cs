﻿using Microsoft.AspNetCore.Mvc;
namespace MOQ.Controller.Controller
{
    [ApiController]
    [Route("api/[controller]")]
    public class MyController : ControllerBase
    {
        private readonly IService _service;
        public MyController(IService service)
        {
            _service = service;
        }
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var data = await _service.GetDataAsync();
            return Ok(data);
        }
    }
    public interface IService
    {
        Task<string> GetDataAsync();
    }
    public class MyService : IService
    {
        public Task<string> GetDataAsync()
        {
            return Task.FromResult("Test data");
        }
    }
}
