# Unit Test

This repository contains examples and setups for unit testing in .NET using MSTest and xUnit.

## Project Structure

- **MSTest/** - Unit testing using MSTest framework.
- **xUnit/unit-testing-using-dotnet-test/** - Unit testing using xUnit and `dotnet test`.

## Requirements

- .NET Core / .NET Framework
- MSTest or xUnit

## Setup & Usage

1. Clone the repository:
   ```sh
   git clone https://gitlab.com/your-repo/unit-test.git
   ```
2. Navigate into the test directory:
   ```sh
   cd MSTest  # or xUnit/unit-testing-using-dotnet-test
   ```
3. Run the tests:
   ```sh
   dotnet test
   ```

## Contribution

Contributions are welcome! Feel free to fork the repository and submit a pull request.

## License

This project is licensed under the MIT License.
